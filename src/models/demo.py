import torch
import torch.nn as nn
import torchvision

m = nn.LogSoftmax(dim=1)
loss = nn.NLLLoss()
# input is of size N x C = 3 x 5
input = torch.randn(3, 5, requires_grad=True)
# each element in target has to have 0 <= value < C
target = torch.tensor([1, 0, 4])
pred = m(input)

print(pred)
output = loss(m(input), target)
